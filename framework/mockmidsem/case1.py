import sys

try:
    from mock_midsem import submission
except:
    print("Cannot import submission from mock_midsem")
else:
    inputs = [i.split() for i in ["+ 1 2", "+ 1 1.5", "- 0 10", "+ 1.2 0.9"]]
    outputs = ["3", "2.5", "-10", "2.1"]
    for args, exp in zip(inputs, outputs):
        out = submission(*args)
        if out != exp:
            print(f"Input   : {args}")
            print(f"Expected: {exp}")
            print(f"Got     : {out}")
            sys.exit(1)
    print("OK! Case1 passed")
